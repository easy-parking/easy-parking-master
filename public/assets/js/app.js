var Laravel = {
    initialize: function() {
        this.methodLinks = $('a[data-method]');
 
        this.registerEvents();
    },
 
    registerEvents: function() {
        this.methodLinks.on('click', this.handleMethod);
    },
 
    handleMethod: function(e) {
        var link = $(this);
        var httpMethod = link.data('method').toUpperCase();
        var form;
 
        if ( $.inArray(httpMethod, ['PUT', 'DELETE']) === - 1 ) {
            return;
        }
 
        if ( link.data('confirm') ) {
            if ( ! Laravel.verifyConfirm(link) ) {
                return false;
            }
        }
 
        form = Laravel.createForm(link);
        form.submit();
 
        e.preventDefault();
    },
 
    verifyConfirm: function(link) {
        return confirm(link.data('confirm'));
    },
 
    createForm: function(link) {
        var form = $('<form>', {
            'method': 'POST',
            'action': link.attr('href')
        });

        var token = $('<input>', {
            'type': 'hidden',
            'name': '_token',
            'value': $('meta[name="_token"]').attr('content')
        });

        var hiddenInput = $('<input>', {
            'name': '_method',
            'type': 'hidden',
            'value': link.data('method')
        });

        return form.append(token, hiddenInput).appendTo('body');
    }
};

var Form = (function(){

	var _public = {};
	var _private = {};

	_private.ajaxSettings = {
        beforeSubmit: function (arr, $form, options) {
            $form.find("input, select, textarea")
                 .removeClass('form-error')
                 .end()
                 .find('.form-message')
                 .text('');
        },
        success: function (result, status, xhr, $form) {
            if($form.hasClass('form-clear')){
                $form.trigger('reset');
            }
        },
        error: function (result, status, xhr, $form) {
            if (result.responseJSON.errors) {
                $.each(result.responseJSON.errors, function (key, value) {
                  	$form.find("input[name='" + key + "'], textarea[name='" + key + "'], select[name='" + key + "']")
                    	 .addClass('form-error');
                });
            } 

            if (result.responseJSON.errorMessage) {
                $form.find('.form-message')
          		     .text(result.responseJSON.errorMessage);
            }

        }
    }

	_public.submit = function(form){

		form.ajaxSubmit(_private.ajaxSettings);

		return false;

	}

    _public.csrf = function(){

        $.ajaxSetup({
            headers: {
                'X-CSRF-Token': $('meta[name="_token"]').attr('content'),
            },
            cache: true,
        });

    }

	return _public;

}());


var Sensor = (function(){

	var _private = {};
	var _public = {};

	_public.change = function($place) {

		if($place.data('sensor') !== undefined){
			var sensor = $place.data('sensor');
			$place.toggleClass("active");

			$.post(DASHBOARD_URL + "/sensor/update/" + sensor).done(function(data){
				console.log(data);
			}).error(function(data){
				console.log(data);
			});
		}

	}

	_private.formatData = function($places) {

		var $place;
		var arduinos = [];
		var floors = [];

		$places.each(function(){
			$place = $(this);

			if($place.find('.arduino').length > 0){
				arduinos.push({
					col: $place.data('col'),
					line: $place.data('line'),
					floor: $place.data('floor'),
				});

				if($.inArray($place.data('floor'), floors)){
					floors.push($place.data('floor'));
				}
			}
		});

		return {
			arduinos: arduinos,
			floors: floors,
		}

	}

	_public.save = function() {

		var data = _private.formatData($('.places .place'));

		$.post(DASHBOARD_URL + "/sensor/save", data).done(function(data){

		});

	}

	return _public;

}());

$(document).ready(function(){
		
	Laravel.initialize();

	Form.csrf();

	$('body').on('submit', 'form.form-ajax', function () {

        return Form.submit($(this));

    });

  	$(".arduinos .arduino").draggable({
		revert: true,
    	cursor: "move",
  	});

    $(".places .place").droppable({
    	accept: ".arduinos .arduino",
		drop: function( event, ui ) {
	    	$(this).append(ui.draggable);
		}
    });

	$(".arduinos-save").click(function(event) {
		event.preventDefault();

		Sensor.save();
	});
  	
  	$(".places .place").click(function(){
  		Sensor.change($(this));
  	});


	var content = $('.right-side').css({
		'height': $('.left-side').height(),
	});

	$(window).resize(function(){
  		content.css({
  			'height': $('.left-side').height(),
  		})
  	});

  	$(document).ajaxComplete(function(event, xhr, settings){

  		var redirect = $.parseJSON(xhr.responseText).redirect;

  		if(redirect !== undefined) {
  			window.location = redirect;
  		}

  	});

});
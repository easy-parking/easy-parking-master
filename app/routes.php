<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
*/

Route::get('/', array(
	'as' => 'home',
	'before' => 'guest',
	'uses' => 'HomeController@index',
));

/*
|--------------------------------------------------------------------------
| Auth Routes
|--------------------------------------------------------------------------
|
*/

Route::get('/auth', array(
	'as' => 'auth',
	'uses' => 'AuthController@index',
));

Route::get('/auth/create', array(
	'as' => 'auth.create',
	'uses' => 'AuthController@create',
));

Route::post('/auth', array(
	'as' => 'auth.store',
	'uses' => 'AuthController@store',
));

Route::post('/auth/login', array(
	'as' => 'auth.login',
	'uses' => 'AuthController@login',
));

Route::get('/auth/logout', array(
	'as' => 'auth.logout',
	'uses' => 'AuthController@logout',
));


/*
|--------------------------------------------------------------------------
| Dashboard Routes
|--------------------------------------------------------------------------
|
*/

Route::get('/dashboard', array(
	'as' => 'dashboard',
	'before' => 'auth',
	'uses' => 'DashboardController@index',
));

Route::get('/dashboard/sensor', array(
	'as' => 'sensor',
	'before' => 'auth',
	'uses' => 'SensorController@index',
));

Route::post('/dashboard/sensor/update/{id}', array(
	'as' => 'sensor.update',
	'before' => 'auth',
	'uses' => 'SensorController@update',
));

Route::post('/dashboard/sensor/save', array(
	'as' => 'sensor.save',
	'before' => 'auth',
	'uses' => 'SensorController@save',
));


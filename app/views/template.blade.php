<?php

$assets = array(
    'javascript' => array(
        'assets/vendor/jquery/dist/jquery.min.js',
        'assets/vendor/jquery-ui/jquery-ui.min.js',
        'assets/vendor/bootstrap/dist/js/bootstrap.min.js',
        'assets/vendor/adminlte/js/AdminLTE/app.js',
        'assets/vendor/jquery-form/jquery.form.js',
        'assets/js/app.js',
    ),
    'stylesheet' => array(
        'assets/vendor/normalize.css/normalize.css',
        'assets/vendor/bootstrap/dist/css/bootstrap.min.css',
        'assets/vendor/adminlte/css/ionicons.min.css',
        'assets/vendor/adminlte/css/font-awesome.min.css',
        'assets/vendor/adminlte/css/AdminLTE.css',
        'assets/css/app.css',
    ),
);

?>

<!doctype html>
<html lang="pt-BR">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>{{ $pageTitle or 'Easy Parking'}}</title>

        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="shortcut icon" href="{{ URL::to('img/favicon.ico') }}" type="image/x-icon">
        <link rel="icon" href="{{ URL::to('img/favicon.ico') }}" type="image/x-icon">

        @foreach ($assets['stylesheet'] as $asset)
            {{ HTML::style($asset) }}
        @endforeach

        <script> var BASE_URL = "{{ URL::to('/') }}";</script>
        <script> var DASHBOARD_URL = "{{ route('dashboard') }}";</script>

    </head>
    <body class="skin-blue {{ $pageClass or '' }}">

        @if(isset($pageClass) && $pageClass == 'auth'){
        
            <h1 class="easy-parking-welcome">Bem vindo ao Easy Parking</h1>
            @yield('content')

        @else
            @include('header')

            <div class="wrapper row-offcanvas row-offcanvas-left">
                <aside class="left-side sidebar-offcanvas">
                    @include('menu')
                </aside>
                <aside class="right-side">
                    @yield('content')
                </aside>
            </div>

        @endif

        @foreach ($assets['javascript'] as $asset)
            {{ HTML::script($asset) }}
        @endforeach
        
    </body>
</html>
@if(isset($sensors[1][$i][$j]))
	<div class="place {{ $sensors[1][$i][$j]->status_id == 2 ? 'active' : '' }}" data-sensor="{{ $sensors[1][$i][$j]->id }}" style="width: {{100/$col}}%; height: {{100/$col}}%;">
		{{ $sensors[1][$i][$j] }}
	</div>
@else
	<div class="place" data-line="{{ $i }}"  data-col="{{ $j }}"  data-floor="1" style="width: {{100/$col}}%; height: {{100/$col}}%;"></div>
@endif
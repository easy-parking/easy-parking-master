@extends('template')

@section('content')

	<section>
		<aside class="arduinos">
			@for($i = 0; $i < 10; $i++)
				<div class="arduino"></div>
			@endfor
		</aside>
		<article class="places">
			@for($i = 1; $i <= $col; $i++)
				@for($j = 1; $j <= $lines; $j++)
					@include('sensor.place')
				@endfor
			@endfor
		</article>

		{{ link_to("#", "Salvar", array('class' => 'arduinos-save')) }}
	</section>

@stop
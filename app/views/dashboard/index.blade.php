@extends('template')

@section('content')
	
	<section>
		<article class="full places">
			@for($i = 1; $i <= $col; $i++)
				@for($j = 1; $j <= $lines; $j++)
					@include('sensor.place')
				@endfor
			@endfor
		</article>
	</section>

@stop
<header class="header">
    {{ link_to_route('dashboard', 'Easy Parking', NULL, array('class' => 'logo')) }}
    <nav class="navbar navbar-static-top" role="navigation">
        <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>
        <div class="navbar-right">
            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="glyphicon glyphicon-user"></i>
                        <span>{{ $user->first_name . ' ' . $user->last_name }} <i class="caret"></i></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="user-header bg-light-blue">
                            {{ HTML::image('assets/img/user.png', $user->first_name . ' ' . $user->last_name, array('class' => 'img-circle')) }}
                            <p>
                                {{ $user->first_name . ' ' . $user->last_name }} 
                                <small>Usuario desde {{ date('d/m/Y', strtotime($user->created_at)) }}</small>
                            </p>
                        </li>
                        <li class="user-footer">
                            <!-- <div class="pull-left">
                                <a href="#" class="btn btn-default btn-flat">Profile</a>
                            </div> -->
                            <div class="pull-right">
                                {{ link_to_route('auth.logout', 'Logout', NULL, array('class' => 'btn btn-default btn-flat')) }}
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
<section class="sidebar">
        <div class="user-panel">
            <div class="pull-left image">
                {{ HTML::image('assets/img/user.png', $user->first_name . ' ' . $user->last_name, array('class' => 'img-circle')) }}
            </div>
            <div class="pull-left info">
                <p>{{ $user->first_name . ' ' . $user->last_name }}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <ul class="sidebar-menu">
            <li class="menu-active treeview">
                <a href="#">
                    <i class="fa fa-dashboard"></i> 
                    <span>Dashboard</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li>
                    	<a href="{{ route('sensor') }}">
                    		<i class="fa fa-angle-double-right"></i> 
                    		Editar estacionamento
                		</a>
            		</li>
                </ul>
            </li>
        </ul>
</section>

@extends('template')

@section('content')

	<div class="form-box" id="login-box">
	    <div class="header">Criar conta</div>
	    {{ Form::open(array('route' => 'auth.store', 'class' => 'form-ajax')) }}
	        <div class="body bg-gray">
	            <div class="form-group">
	                <input type="text" name="firstName" class="form-control" placeholder="Nome"/>
	            </div>
	             <div class="form-group">
	                <input type="text" name="lastName" class="form-control" placeholder="Sobrenome"/>
	            </div>
	            <div class="form-group">
	                <input type="text" name="email" class="form-control" placeholder="Email"/>
	            </div>
	            <div class="form-group">
	                <input type="password" name="password" class="form-control" placeholder="Senha"/>
	            </div>
	            <div class="form-group">
	                <input type="password" name="passwordConfirm" class="form-control" placeholder="Senha novamente"/>
	            </div>
	            <span class="form-message"></span>
	        </div>
	        <div class="footer">                    
	            <button type="submit" class="btn bg-olive btn-block">Cadastrar</button>
	            {{ link_to_route('auth', 'Já possuo uma conta', NULL, array('class' => 'text-center')) }}
	        </div>
	   {{ Form::close() }}
	</div>

@stop
@extends('template')

@section('content')
	
	<div class="form-box" id="login-box">
            <div class="header">Entre no sistema</div>
            {{ Form::open(array('route' => 'auth.login', 'class' => 'form-ajax')) }}
                <div class="body bg-gray">
                    <div class="form-group">
                        <input type="text" name="email" class="form-control" placeholder="Email"/>
                    </div>
                    <div class="form-group">
                        <input type="password" name="password" class="form-control" placeholder="Senha"/>
                    </div>          
                    <div class="form-group">
                        <input type="checkbox" name="remember"/> Lembrar
                    </div>
                    <span class="form-message"></span>
                </div>
                <div class="footer">                                                               
                    <button type="submit" class="btn bg-olive btn-block">Entrar</button>  
                    {{ link_to_route('auth.create', 'Registrar', NULL, array('class' => 'text-center')) }}
                </div>
			{{ Form::close() }}
        </div>

@stop
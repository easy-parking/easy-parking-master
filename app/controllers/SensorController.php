<?php

class SensorController extends BaseController {

	public function index() {

		return View::make('sensor/index')
				   ->with("col", 5)
				   ->with('lines', 5);

	}

	public function update($id) {

		$sensor = Sensor::find($id);

		$status = Status::where('name', '=', 'Ativo')
						->first();

		if($sensor->status->name == 'Ativo') {
			$status = Status::where('name', '=', 'Ocupado')
							->first();
		}

		$sensor->status_id = $status->id;
		$sensor->save();

		return Response::json(array(
			'success' => 'true',
			'sensor' => $sensor,
		), 200);
		
	}

	private function cleanFloors($floors) {
		foreach ($floors as $floor) {
			$sensor = Sensor::where('floor', '=', $floor);
			$sensor->delete();
		}
	}

	public function save() {

		$this->cleanFloors(Input::get("floors"));
		$status = Status::where('name', '=', 'ativo')->first();

		foreach(Input::get('arduinos') as $arduino) {

			Sensor::create(array(
				'line' => $arduino['line'],
				'col' => $arduino['col'],
				'floor' => $arduino['floor'],
				'status_id' => $status->id,
			));

		}

		return Response::json(array(
			'success' => 'true',
			'redirect' => route('dashboard'),
		), 200); 

	}

}

<?php

class AuthController extends BaseController {

	public function index() {

		return View::make('auth/login')
				   ->with('pageClass', 'auth');

	}

	public function create() {

		return View::make('auth/create')
				   ->with('pageClass', 'auth');

	}

	public function store() {

		$rules = array(
            'firstName' => 'required',
            'lastName' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
            'passwordConfirm' => 'required|same:password',
        );

		$validator = Validator::make(Input::all(), $rules);
		
		if($validator->fails()) {

			return Response::json(array(
				'success' => FALSE,
            	'errors' => $validator->getMessageBag()->toArray()
            ), 400);

		}

		try{

			Sentry::createUser(array(
				'first_name' => Input::get('firstName'),
				'last_name' => Input::get('lastName'),
		        'email'     => Input::get('email'),
		        'password'  => Input::get('password'),
		        'activated' => TRUE,
		    ));

			return Response::json(array(
				'success' => TRUE,
            	'redirect' => route('auth'),
            ), 200);

		} catch(Exception $e) {
			
			return Response::json(array(
				'success' => FALSE,
            	'errorMessage' => 'Ocorreu um erro inesperado, por favor, tente mais tarde.',
            ), 400);

		}

	}

	public function login() {

		$rules = array(
            'email' => 'required',
            'password' => 'required',
        );

		$validator = Validator::make(Input::all(), $rules);
		
		if($validator->fails()) {
			return Response::json(array(
				'success' => FALSE,
            	'errors' => $validator->getMessageBag()->toArray()
            ), 400);
		}

		$credentials = array(
	        'email'    => Input::get('email'),
	        'password' => Input::get('password'),
	    );
	    
		$remember = Input::get('remember') || FALSE;

		try{

		    Sentry::authenticate($credentials, $remember);

		    return Response::json(array(
				'success' => TRUE,
            	'redirect' => route('dashboard'),
            ), 200);

		} catch(Exception $e) {

			return Response::json(array(
				'success' => FALSE,
            	'errorMessage' => 'Usuario não existente',
            ), 400);

		}
	}
	
	public function logout() {

		Sentry::logout();

		return Redirect::route('auth');

	}

}

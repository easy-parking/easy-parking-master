<?php

class DashboardController extends BaseController {

	private function formatSensors($sensors) {

		$formatedSensors = array();

		foreach ($sensors as $sensor) {
			$formatedSensors[$sensor->floor][$sensor->line][$sensor->col] = $sensor;
		}

		return $formatedSensors;
	
	}

	public function index() {

		$sensors = Sensor::all();

		return View::make('dashboard/index')
				   ->with("col", 5)
				   ->with('lines', 5)
				   ->with('sensors', $this->formatSensors($sensors));
		
	}
	
}

<?php

class Status extends Eloquent {

	protected $table = 'status';

	protected $guarded = array();

	public function sensor(){
		return $this->hasOne("Sensor");
	}

}

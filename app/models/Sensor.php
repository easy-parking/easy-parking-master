<?php

class Sensor extends Eloquent {

	protected $table = 'sensor';

	protected $guarded = array();

	public function status() {
		return $this->belongsTo('Status');
	}

	public function type() {
		return $this->belongsTo('Type');
	}

}
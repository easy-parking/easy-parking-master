<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParkingTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('parking', function($table) {
		    $table->increments('id');
		    $table->string('name');
		    $table->string('cnpj');
		    $table->decimal('latitude', 10, 8);
		    $table->decimal('longitude', 10, 8);
		    $table->integer('user_id');
		    $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('parking');
	}

}

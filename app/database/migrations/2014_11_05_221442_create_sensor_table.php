<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSensorTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

		Schema::create('sensor', function($table) {
		    $table->increments('id');
		    $table->string('floor');
		    $table->string('line');
		    $table->string('col');
		    $table->integer('status_id');
		    $table->text('description');
		    $table->integer('parking_id');
		    $table->integer('type_id');
		    $table->timestamps();
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sensor');
	}

}

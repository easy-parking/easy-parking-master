<?php

class StatusTableSeeder extends Seeder {

    public function run() {
        DB::table('status')->delete();

        Status::create(array(
        	'name' => 'Ativo',
    	));

    	Status::create(array(
        	'name' => 'Ocupado',
    	));

    }

}
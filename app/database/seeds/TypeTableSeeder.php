<?php

class TypeTableSeeder extends Seeder {

    public function run() {
        DB::table('type')->delete();

     
        Type::create(array(
            'name' => 'Normal',
        ));

        Type::create(array(
            'name' => 'Especial',
        ));

    }

}
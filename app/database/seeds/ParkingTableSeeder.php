<?php

class ParkingTableSeeder extends Seeder {

    public function run() {
        DB::table('parking')->delete();

        Parking::create(array(
        	'name' => 'Estacionamento Massa',
        	'cnpj' => '38.388.673/0001-33',
        	'latitude' => '-29.994777',
        	'longitude' => '-51.1042974',
        	'user_id' => 1,
    	));

     
    }

}